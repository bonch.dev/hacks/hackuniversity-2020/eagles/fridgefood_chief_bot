package main

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	tb "gitlab.com/bonch.dev/go-lib/telebot"
	"gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_chief_bot/handlers"
)

var logger *log.Logger

func init() {
	logger = log.New()

	if err := godotenv.Load(); err != nil {
		logger.Fatalf("Error .env loading: %v", err.Error())
	} else {
		logger.Info(".env file loaded")
	}

	if os.Getenv("DEBUG") == "true" {
		logger.SetLevel(log.DebugLevel)
	} else {
		logger.SetLevel(log.ErrorLevel)
	}
}

func main() {
	client := getDBClient(logger)
	bot := getTbBot(logger)

	defHandler := handlers.NewHandler(bot, client, logger)

	bot.Handle("/start", handlers.StartHandle(defHandler))

	bot.Handle(&handlers.Balance, handlers.BalanceHandle(defHandler))
	bot.Handle(&handlers.AddReceipt, handlers.AddReceiptHandle(defHandler))
	bot.Handle(&handlers.MyReceipts, handlers.MyReceiptsHandle(defHandler))

	bot.Handle(&handlers.Done, handlers.AddReceiptDoneHandle(defHandler))

	bot.Handle(tb.OnText, handlers.TextHandle(defHandler))
	bot.Handle(tb.OnVideo, handlers.TextHandle(defHandler))
	bot.Handle(tb.OnPhoto, handlers.TextHandle(defHandler))

	bot.Start()
}

func getDBClient(logger *log.Logger) *sql.DB {
	var (
		clientConfig string
		driver       = os.Getenv("DRIVER")
		driverHost   = os.Getenv("DRIVER_HOST")
		driverUser   = os.Getenv("DRIVER_USER")
		driverPass   = os.Getenv("DRIVER_PASS")
		driverDb     = os.Getenv("DRIVER_DB")
	)

	switch driver {
	case "mysql":
		clientConfig = fmt.Sprintf("%v:%v@tcp(%v)/%v", driverUser, driverPass, driverHost, driverDb)
	default:
		clientConfig = fmt.Sprintf("%v:%v@%v/%v", driverUser, driverPass, driverHost, driverDb)
	}

	logger.Infof("Connecting to DB with conf: %v", clientConfig)

	client, e := sql.Open(driver, clientConfig)
	if e != nil {
		logger.Errorln(e)
		logger.Fatalf("Can't load DB with this data: %v", clientConfig)
	}

	if err := client.Ping(); err != nil {
		logger.Fatal(err)
	}

	return client
}

func getTbBot(logger *log.Logger) *tb.Bot {
	var (
		//listenURL = os.Getenv("URL")
		//port      = os.Getenv("PORT")
		//publicURL = os.Getenv("PUBLIC_URL")
		token = os.Getenv("TOKEN")
	)

	//webhook := &tb.Webhook{
	//    Listen:   listenURL + ":" + port,
	//    Endpoint: &tb.WebhookEndpoint{PublicURL: publicURL},
	//}

	settings := tb.Settings{
		Token:  token,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
		//Poller: webhook,
	}

	bot, err := tb.NewBot(settings)
	if err != nil {
		logger.Fatalln(err)
	}
	return bot
}
