package handlers

import (
    "fmt"

    tb "gitlab.com/bonch.dev/go-lib/telebot"
    "gitlab.com/bonch.dev/hacks/hackuniversity-2020/eagles/fridgefood_chief_bot/models"
)

func BalanceHandle(handler *Handler) func(m *tb.Message) {
    return (&balanceHandler{*handler}).Handle
}

type balanceHandler struct {
    Handler
}

func (h *balanceHandler) Handle(m *tb.Message) {
    var user *models.User

    user, err := h.getAuthUserById(m.Sender)
    if err != nil {
        if _, err := h.bot.Send(m.Sender, userNotFound); err != nil {
            h.logger.Errorf("[text] [user not found] [send error]: %v", err)
        }

        return
    }

    if _ ,err := h.bot.Send(m.Sender, fmt.Sprintf(userBalance, user.Balance)); err != nil {
        h.logger.Error(err)
    }
}
